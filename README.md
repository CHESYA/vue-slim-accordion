# vue-slim-accordion

Simple accordion component for Vue.js


## Features

- Make Accordion Component on your project


## Installing

```bash
$ npm install vue-slim-accordion
```


## Usage

```js
<template>
<div class="wrap">
  <Accordion>
    <template slot="title">
      Section 1 title
    </template>
    <template slot="content">
      <p>
        Lorem ipsum dolor sit amet,
        consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut
        labore et dolore magna aliqua.
      </p>
    </template>
  </Accordion>
  <Accordion>
    <template slot="title">
      Section 2 title
    </template>
    <template slot="content">
      <p>
        Lorem ipsum dolor sit amet,
        consectetur adipiscing elit,
        sed do eiusmod tempor incididunt ut
        labore et dolore magna aliqua.
      </p>
    </template>
  </Accordion>
</template>

<script>
import Accordion from 'vue-slim-accordion';

export default {
  components: {
    Accordion
  }
};
</script>
```


## Author

**vue-slim-accordion** © [CHE5YA](https://github.com/che5ya), Released under the [MIT](./LICENSE) License.<br>

> GitHub [@che5ya](https://github.com/che5ya) · Facebook [@che5ya](https://www.facebook.com/che5ya) · Twitch [@chesya_](https://www.twitch.tv/chesya_)


## License

[MIT](LICENSE)
